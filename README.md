# Turnkey docker-compose configurations for Duniter-v2s

1. Clone the repo

        $ git clone https://git.duniter.org/pini-gh/docker-duniter-v2s/

2. Go into the wanted configuration folder

        $ cd docker-duniter-v2s/mirror+smith

3. Edit the `.env` file to set your nodes' names and DNS.

    Keep the setting `LETSENCRYPT_TEST=true`.

4. Start you nodes
    
        $ docker-compose up -d

5. Check the letsencrypt logs
    
        $ docker logs letsencrypt
    
    The above command should show the TLS certificate creation:
    
        -----BEGIN CERTIFICATE-----
        MIIGTTCCBTWgAwIBAgITAPoqCxRbhEkWl2+NdDOIsb8EYTANBgkqhkiG9w0BAQsF
        ADBZMQswCQYDVQQGEwJVUzEgMB4GA1UEChMXKFNUQUdJTkcpIExldCdzIEVuY3J5
        cHQxKDAmBgNVBAMTHyhTVEFHSU5HKSBBcnRpZmljaWFsIEFwcmljb3QgUjMwHhcN
        ...
        mJGCFbMH1K/ivOGGdyBG7EQENhKQbONYntM5u4lZUEsbNZ3MHtTORozFn9xrMG44
        dGCFvxTC0ehDMp8gzfVLc1JRDkM6z6624+0J/0y1YbrQIWtTWmFuWqzVXIzJqeVC
        XkLo2Me858XiNhzI7bsnbejua4bIf5togT02WmFscvGq
        -----END CERTIFICATE-----
        [Thu Apr  6 12:49:41 UTC 2023] Your cert is in  /etc/acme.sh/staging/gdev.example.org/gdev.example.org.cer 
        [Thu Apr  6 12:49:41 UTC 2023] Your cert key is in  /etc/acme.sh/staging/gdev.example.org/gdev.example.org.key 
        [Thu Apr  6 12:49:41 UTC 2023] The intermediate CA cert is in  /etc/acme.sh/staging/gdev.example.org/ca.cer 
        [Thu Apr  6 12:49:41 UTC 2023] And the full chain certs is there:  /etc/acme.sh/staging/gdev.example.org/fullchain.cer 
        [Thu Apr  6 12:49:41 UTC 2023] Installing cert to:/etc/nginx/certs/_test_gdev.example.org/cert.pem
        [Thu Apr  6 12:49:41 UTC 2023] Installing CA to:/etc/nginx/certs/_test_gdev.example.org/chain.pem
        [Thu Apr  6 12:49:41 UTC 2023] Installing key to:/etc/nginx/certs/_test_gdev.example.org/key.pem
        [Thu Apr  6 12:49:41 UTC 2023] Installing full chain to:/etc/nginx/certs/_test_gdev.example.org/fullchain.pem
        Reloading nginx proxy (reverse_proxy)...
        2023/04/06 12:49:44 Generated '/etc/nginx/conf.d/default.conf' from 5 containers
        2023/04/06 12:49:45 Contents of /etc/nginx/nginx-stream.conf did not change. Skipping notification ''
        2023/04/06 12:49:45 Contents of /etc/nginx/conf.d/default.conf did not change. Skipping notification ''
        2023/04/06 12:49:45 [notice] 104#104: signal process started
        Sleep for 3600s

    If an error is reported instead, adjust your local network settings so that ports 80 and 443 of your server are reachable from internet and retry.

6. Check the nodes logs

        $ docker logs docker-duniter-v2s_duniter-smith_1

    You should see the synchronisation going along:

        2023-04-06 13:23:17 ⚙️  Syncing 48.1 bps, target=#1618718 (9 peers), best: #837425 (0x73db…cd92), finalized #837120 (0x9e3c…8d32), ⬇ 6.0kiB/s ⬆ 0.6kiB/s    
        2023-04-06 13:23:22 ⚙️  Syncing 399.6 bps, target=#1618719 (9 peers), best: #839424 (0x98cf…e157), finalized #839168 (0x0ff3…8a6f), ⬇ 148.0kiB/s ⬆ 1.9kiB/s    
        2023-04-06 13:23:27 ⚙️  Syncing 120.3 bps, target=#1618720 (9 peers), best: #840026 (0x81ca…aa87), finalized #839680 (0xcebf…58dc), ⬇ 5.2kiB/s ⬆ 0.5kiB/s    
        2023-04-06 13:23:32 ⚙️  Syncing 327.4 bps, target=#1618721 (9 peers), best: #841664 (0x88ff…a2d3), finalized #841216 (0x5df5…59fb), ⬇ 148.5kiB/s ⬆ 1.7kiB/s    
        2023-04-06 13:23:37 ⚙️  Syncing 134.3 bps, target=#1618722 (9 peers), best: #842336 (0xa4f1…5a4e), finalized #842240 (0xbc4d…0da5), ⬇ 5.2kiB/s ⬆ 0.2kiB/s    
        2023-04-06 13:23:42 ⚙️  Syncing 313.5 bps, target=#1618723 (9 peers), best: #843904 (0xa6f6…de75), finalized #843776 (0x3b00…1314), ⬇ 148.1kiB/s ⬆ 1.6kiB/s    

7. Re-run in production mode

    Once everything is OK, stop and clear the nodes:

        $ docker-compose down -v

    Then edit the `.env` file to set `LETSENCRYPT_TEST=false`, and restart with:

        $ docker-compose up -d

8. Finaly re-check the logs to verify everything is running fine.

